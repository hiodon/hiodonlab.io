Title: Hello Pelican
Date: 2020-11-03
Category: UnCat
Tags: pelican, gitlab
Slug: pelican-on-gitlab-pages



# Hello Pelican!

This site is hosted on GitLab Pages!

The source code of this site is at <https://gitlab.com/pages/pelican>.

Learn about GitLab Pages at <https://pages.gitlab.io>.
